/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia. For licensing terms and
** conditions see http://qt.digia.com/licensing. For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights. These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwindowsstylushandler.h"

#ifndef QT_NO_TABLETEVENT

#include "qwindowscontext.h"
#include "qwindowskeymapper.h"
#include "qwindowswindow.h"
#include "qwindowsscreen.h"
#include <QtGui/QTabletEvent>
#include <QtGui/QScreen>
#include <QtGui/QGuiApplication>
#include <QtGui/QWindow>
#include <QtCore/QDebug>
#include <QtCore/QScopedArrayPointer>
#include <QtCore/QtMath>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget> 

#include <private/qguiapplication_p.h>
#include <private/qhighdpiscaling_p.h>
#include <QtCore/private/qsystemlibrary_p.h>

QT_BEGIN_NAMESPACE

// Forces generation of non-extern GUID structs
#include <initguid.h>

__CRT_UUID_DECL(QWindowsInkTabletInterface2,0x90c91ad2,0xfa36,0x49d6,0x95,0x16,0xce,0x8d,0x57,0x0f,0x6f,0x85);
__CRT_UUID_DECL(QWindowsRealTimeStylusInterface3,0xD70230A3,0x6986,0x4051,0xB5,0x7A,0x1C,0xF6,0x9F,0x4D,0x9D,0xB5);
// RTSCom -> IRealTimeStylus
DEFINE_GUID(IID_IRealTimeStylus,0xA8BB5D22,0x3144,0x4a7b,0x93,0xCD,0xF3,0x4A,0x16,0xBE,0x51,0x3A);
DEFINE_GUID(CLSID_IRealTimeStylus,0xE26B366D,0xF998,0x43ce,0x83,0x6F,0xCB,0x6D,0x90,0x44,0x32,0xB0);
// RTSCom -> IStylusPlugin
DEFINE_GUID(IID_IStylusPlugin,0xA81436D8,0x4757,0x4fd1,0xA1,0x85,0x13,0x3F,0x97,0xC6,0xC5,0x45);
// RTSCom -> IStylusSyncPlugin
DEFINE_GUID(IID_IStylusSyncPlugin,0xA157B174,0x482F,0x4d71,0xA3,0xF6,0x3A,0x41,0xDD,0xD1,0x1B,0xE9);
DEFINE_GUID(IID_IStylusAsyncPlugin,0xA7CCA85A,0x31BC,0x4cd2,0xAA,0xDC,0x32,0x89,0xA3,0xAF,0x11,0xC8);
// Ink Property GUIDs
DEFINE_GUID(GUID_PROP_X,0x598A6A8F,0x52C0,0x4BA0,0x93,0xAF,0xAF,0x35,0x74,0x11,0xA5,0x61);
DEFINE_GUID(GUID_PROP_Y,0xB53F9F75,0x04E0,0x4498,0xA7,0xEE,0xC3,0x0D,0xBB,0x5A,0x90,0x11);
DEFINE_GUID(GUID_PROP_Z,0x735ADB30,0x0EBB,0x4788,0xA0,0xE4,0x0F,0x31,0x64,0x90,0x05,0x5D);
DEFINE_GUID(GUID_PROP_PACKETSTATUS,0x6E0E07BF,0xAFE7,0x4CF7,0x87,0xD1,0xAF,0x64,0x46,0x20,0x84,0x18);
DEFINE_GUID(GUID_PROP_TIMERTICK,0x436510C5,0xFED3,0x45D1,0x8B,0x76,0x71,0xD3,0xEA,0x7A,0x82,0x9D);
DEFINE_GUID(GUID_PROP_SERIALNUMBER,0x78A81B56,0x0935,0x4493,0xBA,0xAE,0x00,0x54,0x1A,0x8A,0x16,0xC4);
DEFINE_GUID(GUID_PROP_NORMALPRESSURE,0x7307502D,0xF9F4,0x4E18,0xB3,0xF2,0x2C,0xE1,0xB1,0xA3,0x61,0x0C);
DEFINE_GUID(GUID_PROP_TANGENTPRESSURE,0x6DA4488B,0x5244,0x41EC,0x90,0x5B,0x32,0xD8,0x9A,0xB8,0x08,0x09);
DEFINE_GUID(GUID_PROP_BUTTONPRESSURE,0x8B7FEFC4,0x96AA,0x4BFE,0xAC,0x26,0x8A,0x5F,0x0B,0xE0,0x7B,0xF5);
DEFINE_GUID(GUID_PROP_XTILTORIENTATION,0xA8D07B3A,0x8BF0,0x40B0,0x95,0xA9,0xB8,0x0A,0x6B,0xB7,0x87,0xBF);
DEFINE_GUID(GUID_PROP_YTILTORIENTATION,0x0E932389,0x1D77,0x43AF,0xAC,0x00,0x5B,0x95,0x0D,0x6D,0x4B,0x2D);
DEFINE_GUID(GUID_PROP_AZIMUTHORIENTATION,0x029123B4,0x8828,0x410B,0xB2,0x50,0xA0,0x53,0x65,0x95,0xE5,0xDC);
DEFINE_GUID(GUID_PROP_ALTITUDEORIENTATION,0x82DEC5C7,0xF6BA,0x4906,0x89,0x4F,0x66,0xD6,0x8D,0xFC,0x45,0x6C);
DEFINE_GUID(GUID_PROP_TWISTORIENTATION,0x0D324960,0x13B2,0x41E4,0xAC,0xE6,0x7A,0xE9,0xD4,0x3D,0x2D,0x3B);
DEFINE_GUID(GUID_PROP_PITCHROTATION,0x7F7E57B7,0xBE37,0x4BE1,0xA3,0x56,0x7A,0x84,0x16,0x0E,0x18,0x93);
DEFINE_GUID(GUID_PROP_ROLLROTATION,0x5D5D5E56,0x6BA9,0x4C5B,0x9F,0xB0,0x85,0x1C,0x91,0x71,0x4E,0x56);
DEFINE_GUID(GUID_PROP_YAWROTATION,0x6A849980,0x7C3A,0x45B7,0xAA,0x82,0x90,0xA2,0x62,0x95,0x0E,0x89);
DEFINE_GUID(GUID_PROP_WIDTH,0xBAABE94D,0x2712,0x48F5,0xBE,0x9D,0x8F,0x8B,0x5E,0xA0,0x71,0x1A);
DEFINE_GUID(GUID_PROP_HEIGHT,0xE61858D2,0xE447,0x4218,0x9D,0x3F,0x18,0x86,0x5C,0x20,0x3D,0xF4);
DEFINE_GUID(GUID_PROP_FINGERCONTACTCONFIDENCE,0xE706C804,0x57F0,0x4F00,0x8A,0x0C,0x85,0x3D,0x57,0x78,0x9B,0xE9);
DEFINE_GUID(GUID_PROP_DEVICE_CONTACT_ID,0x02585B91,0x049B,0x4750,0x96,0x15,0xDF,0x89,0x48,0xAB,0x3C,0x9C);
// Ink Property BSTRs
#define STR_PROP_X                          L"{598A6A8F-52C0-4BA0-93AF-AF357411A561}"
#define STR_PROP_Y                          L"{B53F9F75-04E0-4498-A7EE-C30DBB5A9011}"
#define STR_PROP_Z                          L"{735ADB30-0EBB-4788-A0E4-0F316490055D}"
#define STR_PROP_PAKETSTATUS                L"{6E0E07BF-AFE7-4CF7-87D1-AF6446208418}"
#define STR_PROP_SERIALNUMBER               L"{78A81B56-0935-4493-BAAE-00541A8A16C4}"
#define STR_PROP_NORMALPRESSURE             L"{7307502D-F9F4-4E18-B3F2-2CE1B1A3610C}"
#define STR_PROP_TANGENTPRESSURE            L"{6DA4488B-5244-41EC-905B-32D89AB80809}"
#define STR_PROP_XTILTORIENTATION           L"{A8D07B3A-8BF0-40B0-95A9-B80A6BB787BF}"
#define STR_PROP_YTILTORIENTATION           L"{0E932389-1D77-43AF-AC00-5B950D6D4B2D}"
#define STR_PROP_TWISTORIENTATION           L"{0D324960-13B2-41E4-ACE6-7AE9D43D2D3B}"
#define STR_PROP_WIDTH                      L"{BAABE94D-2712-48F5-BE9D-8F8B5EA0711A}"
#define STR_PROP_HEIGHT                     L"{E61858D2-E447-4218-9D3F-18865C203DF4}"

QWindowsStylusEventPlugin::QWindowsStylusEventPlugin(HWND window) :
    m_refCount(1),
    m_marshaler(0),
    m_window(window)
{
    HRESULT hr = CoCreateFreeThreadedMarshaler(this, &m_marshaler);
    qCDebug(lcQpaTablet) << "CoCreateFreeThreadedMarshaler" << hr;
}

QWindowsStylusEventPlugin::~QWindowsStylusEventPlugin()
{
    if(m_marshaler != 0)
    {
        m_marshaler->Release();
    }
}

inline void getRange(QWindowsInkTabletInterface *tablet, BSTR prop, struct QWindowsTabletRanges::range *range)
{
    int units;
    float resolution;

    HRESULT hr = tablet->getPropertyMetrics(prop, &range->min, &range->max, &units, &resolution);
    qDebug() << "getPropMetrics hr=" << hr << " min=" << range->min << " max=" << range->max << " units=" << units << " resolution=" << resolution;

    if(hr != S_OK)
    {
        qDebug() << "Failed to get property metrics";

        // Set some defaults that will not cause a /0 fault
        range->min = 0;
        range->max = 1;
    }
}

QWindowsTabletInfo& QWindowsStylusEventPlugin::queryTablet(QWindowsRealTimeStylusInterface *realTimeStylus, TabletId tcid)
{
	if(m_tabletInfos.contains(tcid))
		return m_tabletInfos[tcid];
	
	// Get IInkTablet
	QWindowsInkTabletInterface *tablet;
	HRESULT hr = realTimeStylus->GetTabletFromTabletContextId(tcid, &tablet);
	QWindowsTabletInfo info;
	
	info.format = {false};
	qDebug() <<"queryTablet() getTabletByTabletId("<<tcid<<") "<< hr;
	
	QWindowsInkTabletInterface2 *tablet2;
	tablet->QueryInterface(&tablet2);

	if (FAILED(tablet2->get_DeviceKind(&info.deviceKind))) {
		info.deviceKind = Mouse;
	}

	ULONG numProperties;
	PacketProperty *properties;
	
	hr = realTimeStylus->getPacketDescriptionData(tcid, &info.scaleX, &info.scaleY, &numProperties, &properties);
	
	qDebug() << QString::fromStdString("queryTablet() getPacketDescriptionData %1 scaleX=%2 scaleY=%3 numProps=%4").arg(hr).arg(info.scaleX).arg(info.scaleY).arg(numProperties);
	
	if(hr == S_OK)
	{
		for(ULONG i = 0; i < numProperties; i++)
		{
			QWindowsTabletRanges::range *range = nullptr;
			if(IsEqualGUID(properties[i].guid, GUID_PROP_X))
			{
				info.format.x = true;
				qDebug() << "queryTablet() Has GUID_X";
				range = &info.ranges.x;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_Y))
			{
				info.format.y = true;
				qDebug() << "queryTablet() Has GUID_Y";
				range = &info.ranges.y;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_Z))
			{
				info.format.z = true;
				qDebug() << "queryTablet() Has GUID_Z";
				range = &info.ranges.z;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_NORMALPRESSURE))
			{
				info.format.pressure = true;
				qDebug() << "queryTablet() Has GUID_PROP_NORMALPRESSURE";
				range = &info.ranges.pressure;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_TANGENTPRESSURE))
			{
				info.format.tangentPressure = true;
				qDebug() << "queryTablet() Has GUID_TANGENTPRESSURE";
				range = &info.ranges.tangentPressure;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_XTILTORIENTATION))
			{
				info.format.xtilt = true;
				qDebug() << "queryTablet() Has GUID_XTILTORIENTATION";
				range = &info.ranges.xtilt;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_YTILTORIENTATION))
			{
				info.format.ytilt = true;
				qDebug() << "queryTablet() Has GUID_YTILTORIENTATION";
				range = &info.ranges.ytilt;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_TWISTORIENTATION))
			{
				info.format.twist = true;
				qDebug() << "queryTablet() Has GUID_TWISTORIENTATION";
				range = &info.ranges.twist;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_SERIALNUMBER))
			{
				info.format.serialNumber = true;
				qDebug() << "queryTablet() Has GUID_SERIALNUMBER";
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_DEVICE_CONTACT_ID))
			{
				info.format.deviceContactId = true;
				qDebug() << "queryTablet() Has GUID_DEVICE_CONTACT_ID";
				range = &info.ranges.contactId;
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_WIDTH))
			{
				info.format.contactWidth = true;
				qDebug() << "queryTablet() Has GUID_WIDTH";
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_HEIGHT))
			{
				info.format.contactHeight = true;
				qDebug() << "queryTablet() Has GUID_HEIGHT";
			}
			else if(IsEqualGUID(properties[i].guid, GUID_PROP_PACKETSTATUS))
			{
				info.format.status = true;
				qDebug() << "queryTablet() Has GUID_PACKETSTATUS";
			}
			
			if(range)
			{
				range->min = properties[i].metrics.min;
				range->max = properties[i].metrics.max;
				
				qDebug() << "queryTablet() min=" << range->min << "max=" << range->max;
			}
		}
	}
	
	m_tabletInfos[tcid] = info;
	return m_tabletInfos[tcid];
}

HRESULT QWindowsStylusEventPlugin::stylusDown(QWindowsRealTimeStylusInterface *piSrcRtp, const StylusInfo *pStylusInfo, ULONG cPropCountPerPkt, LONG *pPacket, LONG **/*ppInOutPkt*/)
{
	qCDebug(lcQpaTablet) << "Down" << (int)pStylusInfo->tabletId << (int)pStylusInfo->cursorId;
	processPackets(piSrcRtp, pStylusInfo, 1, cPropCountPerPkt, pPacket, false, TOUCHEVENTF_DOWN);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::stylusUp(QWindowsRealTimeStylusInterface *piSrcRtp, const StylusInfo *pStylusInfo, ULONG cPropCountPerPkt, LONG *pPacket, LONG **/*ppInOutPkt*/)
{
	qCDebug(lcQpaTablet) << "Up" << (int)pStylusInfo->tabletId << (int)pStylusInfo->cursorId;
	processPackets(piSrcRtp, pStylusInfo, 1, cPropCountPerPkt, pPacket, true, TOUCHEVENTF_UP);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::stylusInRange(QWindowsRealTimeStylusInterface *piSrcRtp, TabletId tcid, CursorId cid)
{
	qCDebug(lcQpaTablet) << "Hover" << tcid << cid;
	QWindowsTabletInfo &info = queryTablet(piSrcRtp, tcid);
	if (info.deviceKind == QTabletDeviceKind::Pen)
		QWindowSystemInterface::handleTabletEnterProximityEvent(tcid, cid, 0);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::stylusOutOfRange(QWindowsRealTimeStylusInterface *piSrcRtp, TabletId tcid, CursorId cid)
{
	QWindowsTabletInfo &info = queryTablet(piSrcRtp, tcid);
	qDebug() << "stylusOutOfRange";
	if (info.deviceKind == QTabletDeviceKind::Pen)
		QWindowSystemInterface::handleTabletLeaveProximityEvent(tcid, cid, 0);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::dataInterest(QWindowsRealTimeStylusDataInterest *pEventInterest)
{
	qCDebug(lcQpaTablet) << "It is interested";
	*pEventInterest = (QWindowsRealTimeStylusDataInterest)(RTSDI_StylusInRange | RTSDI_StylusOutOfRange | RTSDI_StylusDown | RTSDI_StylusUp | RTSDI_InAirPackets | RTSDI_Packets);

	return S_OK;
}

static inline int sign(int x)
{
	return x >= 0 ? 1 : -1;
}

inline QPointF QWindowsStylusEventPlugin::scaleCoordinates(QWindowsRealTimeStylusInterface */*realTimeStylus*/, int coordX, int coordY, TabletId /*tcid*/, const RECT &windowArea)
{
	int screen_idx = QApplication::desktop()->screenNumber(QWindowsCursor::mousePosition());
	QScreen* screen = qApp->screens().at(screen_idx);
	qreal factor = QHighDpiScaling::factor(screen);
	QDpi dpi = QHighDpiScaling::logicalDpi();

	const qreal x = windowArea.left + (float)coordX * dpi.first *factor/2540.0;
	const qreal y = windowArea.top  + (float)coordY * dpi.second*factor/2540.0;
	qDebug() << "SCALE_COORD: ["<<x<<", "<<y<<"] window: "<<windowArea.right-windowArea.left<<", "<<windowArea.bottom-windowArea.top
			 << " factor: "<<factor<<" dpi: "<<dpi;

	return QPointF(x, y);
}

inline Qt::MouseButtons QWindowsStylusEventPlugin::translatePacketStatus(long status) const
{
	enum { QT_PACKETSTATUS_TOUCHING = 0x01, QT_PACKETSTATUS_INVERTED = 0x02,
		   QT_PACKETSTATUS_BARREL = 0x08 };

	Qt::MouseButtons btn = Qt::NoButton;

	if(status & QT_PACKETSTATUS_TOUCHING)
		btn |= Qt::LeftButton;
	if(status & QT_PACKETSTATUS_BARREL)
		btn |= Qt::RightButton;

	return btn;
}

static inline QTabletEvent::PointerType pointerType(const StylusInfo *pStylusInfo)
{
	if (pStylusInfo->invertedCursor) {
		return QTabletEvent::Eraser;
	} else {
		return QTabletEvent::Pen;
	}
}

inline void QWindowsStylusEventPlugin::processPackets(QWindowsRealTimeStylusInterface *realTimeStylus, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, bool isInAir, DWORD dwFlags)
{
	ULONG i;
	ULONG packetSize = cPktBuffLength / cPktCount;

	qDebug() << (isInAir ? "OFF" : "ON") << "SURFACE: Received " << cPktCount << " packets, length:" << cPktBuffLength << ", " << packetSize << " properties";

	QWindowsTabletInfo &info = queryTablet(realTimeStylus, pStylusInfo->tabletId);
	if (info.deviceKind == QTabletDeviceKind::Mouse) return;

	RECT windowArea;
	GetWindowRect(m_window, &windowArea);
	QMargins frame_margins = qApp->focusWindow() ? qApp->focusWindow()->frameMargins() : QMargins();
	windowArea.top  += frame_margins.top();
	windowArea.left += frame_margins.left();
	windowArea.bottom -= frame_margins.top () + frame_margins.bottom();
	windowArea.right  -= frame_margins.left() + frame_margins.right ();

	for(i = 0; i < cPktCount; i++, pPackets += packetSize) {
		long *packetData = pPackets;
		QWindowsStylusPacket packet;

		// Parse the packet
		// MUST BE FIRST
		packet.x = info.format.x ? *(packetData++) : 0;
		// MUST BE SECOND
		packet.y = info.format.y ? *(packetData++) : 0;
		packet.z = info.format.z ? *(packetData++) : 0;
		packet.pressure = info.format.pressure ? *(packetData++) : 0;
		packet.tangentPressure = info.format.tangentPressure ? *(packetData++) : 0;
		packet.xtilt = info.format.xtilt ? *(packetData++) : 0;
		packet.ytilt = info.format.ytilt ? *(packetData++) : 0;
		packet.twist = info.format.twist ? *(packetData++) : 0;
		packet.serialNumber = info.format.serialNumber ? *(packetData++) : 0;
		packet.deviceContactId = info.format.deviceContactId ? *(packetData++) : 0;
		packet.contactWidth = info.format.contactWidth ? *(packetData++) : 0;
		packet.contactHeight = info.format.contactHeight ? *(packetData++) : 0;
		// MUST BE LAST
		packet.status = info.format.status ? *(packetData++) : 0;

		QWindow *target = QGuiApplicationPrivate::tabletPressTarget; // Pass to window that grabbed it.
		const QPointF globalPosF = scaleCoordinates(realTimeStylus, packet.x, packet.y, pStylusInfo->tabletId, windowArea);
		const QPoint globalPos = globalPosF.toPoint();

		qDebug() << "PACKET device=" << ((info.deviceKind == QTabletDeviceKind::Pen) ? "Pen" : (info.deviceKind == QTabletDeviceKind::Touch) ? "Touch" : "Mouse")
				 << ((dwFlags & TOUCHEVENTF_UP) ? " UP" : (dwFlags & TOUCHEVENTF_DOWN) ? " DOWN" : " MOVE")
				 << " x=" << packet.x
				 << " y=" << packet.y
				 << " z=" << packet.z
				 << " pressure=" << packet.pressure
				 << " tang_pressure=" << packet.tangentPressure
				 << " xtilt=" << packet.xtilt/100
				 << " ytilt=" << packet.ytilt/100
				 << " twist=" << packet.twist
				 << " serial_num=" << packet.serialNumber
				 << " contact_id=" << packet.deviceContactId
				 << " contact_w=" << packet.contactWidth
				 << " contact_h=" << packet.contactHeight
				 << " status=" << packet.status
				 << " globposf=" << globalPosF
				 << " target=" << (target ? target->position() : QPoint(-1,-1))
				 << " windowArea=" << windowArea.top;

		if (!target)
			target = QWindowsScreen::windowAt(globalPos, CWP_SKIPINVISIBLE | CWP_SKIPTRANSPARENT);
		if (!target)
			continue;

		const QPoint localPos = target->mapFromGlobal(globalPos);
		// map localPos to device independent pixels
		const QPointF localPosDip = QPointF(QHighDpi::fromNativePixels(localPos, target));
		const QPointF globalPosDip = QHighDpi::fromNativePixels(globalPosF, target);

		if (info.deviceKind == QTabletDeviceKind::Pen)
		{
			// linear mapping from <min, max> to <-1, 1>
			// (val - min) * ((1 - (-1))/(max-min)) + (-1)
			qreal tangential_pressure = info.format.tangentPressure
				? (packet.tangentPressure - info.ranges.tangentPressure.min) * 2 / qreal(info.ranges.tangentPressure.max - info.ranges.tangentPressure.min) - 1
				: 0.0;
			QWindowSystemInterface::handleTabletEvent(target, localPosDip, globalPosDip,
													  pStylusInfo->tabletId, pointerType(pStylusInfo),
													  translatePacketStatus(packet.status),
													  qreal(packet.pressure) / info.ranges.pressure.max,
													  0, 0,
													  tangential_pressure, 0, 0, packet.serialNumber,
													  QWindowsKeyMapper::queryKeyboardModifiers());
		} else if (info.deviceKind == QTabletDeviceKind::Touch) {
			typedef QWindowSystemInterface::TouchPoint QTouchPoint;

			if (!QWindowsContext::instance()->initTouch()) {
				qWarning("Unable to initialize touch handling.");
				return;
			}
			int j = 0;
			bool found = false;
			for (; j < m_touchPoints.size(); ++j) {
				if (m_touchPoints[j].id == packet.deviceContactId) {
					found = true;
					break;
				}
			}

			if (!found) {
				// when point is not found and we have received packet for MOVE touch event
				if (dwFlags & TOUCHEVENTF_MOVE)
					return;
				if (packet.deviceContactId == 0 && (dwFlags & TOUCHEVENTF_UP)) {
					// when receiving packet for pen device on surface pro, while finger is still touching surface,
					// contactId for TOUCHEVENTF_UP will be 0 (even if device supports multitouch)
					j = m_touchPoints.size() - 1;
				} else {
					QTouchPoint point;
					point.id = packet.deviceContactId;
					m_touchPoints << point;
				}
			}
			QTouchPoint &point = m_touchPoints[j];
			point.pressure = info.format.pressure ? qreal(packet.pressure) / info.ranges.pressure.max : 1.0;
			if (packet.contactWidth > 0 && packet.contactHeight > 0) {
				point.area.setSize(QSizeF(packet.contactWidth, packet.contactHeight) / qreal(100.));
			}
			point.area.moveCenter(globalPosF);
			const int targetWidth = windowArea.right - windowArea.left;
			const int targetHeight = windowArea.bottom - windowArea.top;
			QPointF normalPosition = QPointF(globalPosF.x() / targetWidth,
											 globalPosF.y() / targetHeight);
			const bool stationaryPoint = (normalPosition == point.normalPosition);
			point.normalPosition = normalPosition;
			
			if (dwFlags & TOUCHEVENTF_DOWN) {
				point.state = Qt::TouchPointPressed;
			} else if (dwFlags & TOUCHEVENTF_UP) {
				point.state = Qt::TouchPointReleased;
			} else {
				point.state = stationaryPoint
					? Qt::TouchPointStationary
					: Qt::TouchPointMoved;
			}
			QWindowSystemInterface::handleTouchEvent(target, QWindowsContext::instance()->touchDevice(), m_touchPoints, QWindowsKeyMapper::queryKeyboardModifiers());
			if (dwFlags & TOUCHEVENTF_UP) {
				m_touchPoints.removeAt(j);
			}
		}
	}
}

HRESULT QWindowsStylusEventPlugin::packets(QWindowsRealTimeStylusInterface *realTimeStylus, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, ULONG */*pcInOutPkts*/, LONG **/*ppInOutPkts*/)
{
	processPackets(realTimeStylus, pStylusInfo, cPktCount, cPktBuffLength, pPackets, false, TOUCHEVENTF_MOVE);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::inAirPackets(QWindowsRealTimeStylusInterface *realTimeStylus, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, ULONG *, LONG **)
{
	processPackets(realTimeStylus, pStylusInfo, cPktCount, cPktBuffLength, pPackets, true, TOUCHEVENTF_MOVE);
	return S_OK;
}

HRESULT QWindowsStylusEventPlugin::QueryInterface(const IID &riid, LPVOID *ppvObj)
{
#ifdef SYNC
	if((riid == IID_IStylusSyncPlugin) || (riid == IID_IUnknown))
#else
	if((riid == IID_IStylusAsyncPlugin) || (riid == IID_IUnknown))
#endif
	{
		*ppvObj = this;
		AddRef();
		return S_OK;
	}
	else if((riid == IID_IMarshal) && (m_marshaler != NULL))
	{
		return m_marshaler->QueryInterface(riid, ppvObj);
	}

	*ppvObj = NULL;
	return E_NOINTERFACE;
}

ULONG QWindowsStylusEventPlugin::AddRef()
{
	return InterlockedIncrement(&m_refCount);
}

ULONG QWindowsStylusEventPlugin::Release()
{
	ULONG cNewRefCount = InterlockedDecrement(&m_refCount);
	if(cNewRefCount == 0)
	{
		delete this;
	}
	return cNewRefCount;
}

QWindowsWindowStylusHandler::QWindowsWindowStylusHandler(HWND window)
	: m_window(window)
	, m_realTimeStylus(0)
	, m_stylusPlugin(0)
{
	qDebug() << "Creating stylus handler for " << window;
	fflush(stdout);
	HRESULT hr;
	// Init COM objects

	hr = CoCreateInstance(CLSID_IRealTimeStylus, 0, CLSCTX_INPROC_SERVER, IID_IRealTimeStylus, IID_PPV_ARGS_Helper (&m_realTimeStylus));
	qCDebug(lcQpaTablet) << "CoCreateInstance" << hr;

	qDebug() << "CoCreateInstance " << hr;
	if(hr == S_OK && m_realTimeStylus != 0) {
		GUID desiredProperties[] = {
			GUID_PROP_X,
			GUID_PROP_Y,
			GUID_PROP_Z,
			GUID_PROP_NORMALPRESSURE,
			GUID_PROP_TANGENTPRESSURE,
			GUID_PROP_XTILTORIENTATION,
			GUID_PROP_YTILTORIENTATION,
			GUID_PROP_TWISTORIENTATION,
			GUID_PROP_SERIALNUMBER,
			GUID_PROP_DEVICE_CONTACT_ID,
			GUID_PROP_WIDTH,
			GUID_PROP_HEIGHT,
			GUID_PROP_PACKETSTATUS,
		};
		
		// Number of properties in the array
		ULONG cProperties = sizeof(desiredProperties) / sizeof(GUID);

		hr = m_realTimeStylus->setDesiredPacketDescription(cProperties, desiredProperties);
		qDebug() << "set packet desc. hr=" << hr;

		m_stylusPlugin = new QWindowsStylusEventPlugin(m_window);

		#ifdef SYNC
		hr = m_realTimeStylus->addStylusSyncPlugin(0, m_stylusPlugin);
		#else
		hr = m_realTimeStylus->addStylusAsyncPlugin(0, m_stylusPlugin);
		#endif
		qDebug() << "addStylusSyncPlugin hr=" << hr;

		qDebug() << m_window;
		// Attach RTS object to a window
		hr = m_realTimeStylus->put_HWND((HANDLE_PTR)m_window);
		qDebug() << "put HWND hr=" << hr;

		// Register RTS object for receiving multi-touch input.
		QWindowsRealTimeStylusInterface3 *realTimeStylus3 = nullptr;
		hr = m_realTimeStylus->QueryInterface(&realTimeStylus3);
		qDebug() << "QueryInterface() hr=" << hr;

		if (SUCCEEDED(hr)) {
			realTimeStylus3->put_MultiTouchEnabled(TRUE);
			realTimeStylus3->Release();
		}

		setEnabled(true);
	}

	if(hr != S_OK) {
		qDebug() << "Failed to create stylus handler";

		delete m_stylusPlugin;
		m_stylusPlugin = 0;

		if(m_realTimeStylus)
		{
			m_realTimeStylus->Release();
			m_realTimeStylus = 0;
		}
	} else {
		qDebug() << "Stylus handler init'd";
	}

	qDebug() << "end";
}

QWindowsWindowStylusHandler::~QWindowsWindowStylusHandler()
{
    if(m_realTimeStylus)
    {
        if(m_stylusPlugin)
        {
			#ifdef SYNC
			m_realTimeStylus->removeStylusSyncPlugin(0, (QWindowsStylusEventPluginInterface**)&m_stylusPlugin);
			#else
			m_realTimeStylus->removeStylusAsyncPlugin(0, (QWindowsStylusEventPluginInterface**)&m_stylusPlugin);
			#endif

            delete m_stylusPlugin;
        }
        m_realTimeStylus->put_Enabled(false);
        m_realTimeStylus->Release();
    }
}

void QWindowsWindowStylusHandler::setEnabled(bool enabled)
{
    if(m_realTimeStylus)
    {
        qDebug() << "Set enabled to " << enabled << ". hr=" << m_realTimeStylus->put_Enabled(enabled);
    }
}

bool QWindowsWindowStylusHandler::enabled()
{
    if(m_realTimeStylus)
    {
        BOOL enabled;
        HRESULT hr = m_realTimeStylus->get_Enabled(&enabled);

        if(hr == S_OK)
            return enabled;
    }

    return false;
}

QWindowsStylusHandler::QWindowsStylusHandler()
{

}

QWindowsStylusHandler::~QWindowsStylusHandler()
{
    while(!m_windowHandlers.empty())
        removeWindow(m_windowHandlers.begin().key());
}

QWindowsStylusHandler *QWindowsStylusHandler::create()
{
    QWindowsRealTimeStylusInterface *rts = 0;
    HRESULT hr = CoCreateInstance(CLSID_IRealTimeStylus, 0, CLSCTX_INPROC_SERVER, IID_IRealTimeStylus, IID_PPV_ARGS_Helper (&rts));

    if(hr == S_OK && rts) // API supported
        return new QWindowsStylusHandler;
    else
        return 0;
}

QWindowsWindowStylusHandler *QWindowsStylusHandler::addWindow(HWND window)
{
    qDebug() << "starting";

    if(m_windowHandlers.contains(window))
        return m_windowHandlers.value(window);
    else
        return m_windowHandlers[window] = new QWindowsWindowStylusHandler(window);
}

void QWindowsStylusHandler::removeWindow(HWND window)
{
    if(!m_windowHandlers.contains(window))
        return;

    delete m_windowHandlers.value(window, 0);

    m_windowHandlers.remove(window);
}

QString QWindowsStylusHandler::description()
{
    return QStringLiteral("Disabled");
}

// mouse
// finger 3071232

#endif // !QT_NO_TABLETEVENT
