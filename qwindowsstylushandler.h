/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia. For licensing terms and
** conditions see http://qt.digia.com/licensing. For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights. These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QWINDOWSSTYLUSHANDLER_H
#define QWINDOWSSTYLUSHANDLER_H

#include "qtwindowsglobal.h"

#if !defined(QT_NO_TABLETEVENT) && !defined(Q_OS_WINCE)

#include <qpa/qwindowsysteminterface.h>

#include <QtCore/QVector>
#include <QtCore/QPointF>
#include <QtCore/QRect>
#include <QtCore/QHash>

class QTouchDevice;

QT_BEGIN_NAMESPACE

// Information obtained from MSDN and Windows 7.0 SDK.
//__CRT_UUID_DECL(IRealTimeStylus,0xA8BB5D22,0x3144,0x4a7b,0x93,0xCD,0xF3,0x4A,0x16,0xBE,0x51,0x3A)

typedef DWORD TabletId;
typedef DWORD CursorId;
typedef unsigned short SystemEvent;

typedef struct StylusInfo
{
    TabletId tabletId;
    CursorId cursorId;
    BOOL invertedCursor;
} StylusInfo;

typedef struct SYSTEM_EVENT_DATA
{
    BYTE bModifier;
    WCHAR wKey;
    LONG xPos;
    LONG yPos;
    BYTE bCursorMode;
    DWORD dwButtonState;
} SYSTEM_EVENT_DATA;

typedef struct
{
    LONG min;
    LONG max;
    int units;
    FLOAT resolution;
} PropertyMetrics;

typedef struct
{
    GUID guid;
    PropertyMetrics metrics;
} PacketProperty;

typedef void* IInkTablet;
typedef struct QWindowsStylusEventPluginInterface QWindowsStylusEventPluginInterface;

typedef enum QWindowsRealTimeStylusDataInterest
{
    RTSDI_AllData = 0xffffffff,
    RTSDI_None = 0,
    RTSDI_Error = 0x1,
    RTSDI_RealTimeStylusEnabled = 0x2,
    RTSDI_RealTimeStylusDisabled = 0x4,
    RTSDI_StylusNew = 0x8,
    RTSDI_StylusInRange = 0x10,
    RTSDI_InAirPackets = 0x20,
    RTSDI_StylusOutOfRange = 0x40,
    RTSDI_StylusDown = 0x80,
    RTSDI_Packets = 0x100,
    RTSDI_StylusUp = 0x200,
    RTSDI_StylusButtonUp = 0x400,
    RTSDI_StylusButtonDown = 0x800,
    RTSDI_SystemEvents = 0x1000,
    RTSDI_TabletAdded = 0x2000,
    RTSDI_TabletRemoved = 0x4000,
    RTSDI_CustomStylusDataAdded	= 0x8000,
    RTSDI_UpdateMapping	= 0x10000,
    RTSDI_DefaultEvents	= ( ( ( ( ( ( RTSDI_RealTimeStylusEnabled | RTSDI_RealTimeStylusDisabled )  | RTSDI_StylusDown )  | RTSDI_Packets )  | RTSDI_StylusUp )  | RTSDI_SystemEvents )  | RTSDI_CustomStylusDataAdded )
    } 	QWindowsRealTimeStylusDataInterest;

struct QWindowsStylusPacket
{
    long x;
    long y;
    long z;
    long pressure;
    long status;
    long tangentPressure;
    long xtilt;
    long ytilt;
    long twist;
    long serialNumber;
	long deviceContactId;
	long contactWidth;
	long contactHeight;
};

struct QWindowsStylusPacketFormat
{
    bool x;
    bool y;
    bool z;
    bool pressure;
    bool status;
    bool tangentPressure;
    bool xtilt;
    bool ytilt;
    bool twist;
    bool serialNumber;
	bool deviceContactId;
	bool contactWidth;
	bool contactHeight;
};

struct QWindowsTabletRanges
{
    struct range {
        long min = 1, max = 1;
    } x, y, z, pressure, tangentPressure, xtilt, ytilt, twist, contactId;
};

typedef enum QTabletDeviceKind
{
	Mouse	= 0,
	Pen		= (Mouse + 1),
	Touch	= (Pen + 1)
}	QTabletDeviceKind;

struct QWindowsTabletInfo
{
    QWindowsStylusPacketFormat format;
    QWindowsTabletRanges ranges;
	QTabletDeviceKind deviceKind;

    float scaleX;
    float scaleY;
};

//-- NOTE: Below interfaces not included with MinGW
struct QWindowsInkTabletInterface : public IDispatch
{
    STDMETHOD(X1)() = 0;
    STDMETHOD(X2)() = 0;
    STDMETHOD(X3)() = 0;
    STDMETHOD(X4)() = 0;
    STDMETHOD(isPacketPropertySupported)(BSTR, BOOL *) = 0;
    STDMETHOD(getPropertyMetrics)(BSTR, long *, long *, int *unit, float *) = 0;
};

struct QWindowsInkTabletInterface2 : public IDispatch
{
public:
	STDMETHOD(get_DeviceKind)(QTabletDeviceKind *) = 0;
	
};

struct QWindowsRealTimeStylusInterface : public IUnknown
{
    STDMETHOD(get_Enabled)(BOOL *) = 0;
    STDMETHOD(put_Enabled)(BOOL) = 0;
    STDMETHOD(get_HWND)(HANDLE_PTR *) = 0;
    STDMETHOD(put_HWND)(HANDLE_PTR) = 0;
    STDMETHOD(X1)() = 0;
    STDMETHOD(X2)() = 0;
#ifdef SYNC
    STDMETHOD(addStylusSyncPlugin)(ULONG, QWindowsStylusEventPluginInterface *) = 0;
    STDMETHOD(removeStylusSyncPlugin)(ULONG, QWindowsStylusEventPluginInterface **) = 0;
#else
	STDMETHOD(addStylusAsyncPlugin)(ULONG, QWindowsStylusEventPluginInterface *) = 0;
    STDMETHOD(removeStylusAsyncPlugin)(ULONG, QWindowsStylusEventPluginInterface **) = 0;
#endif
    STDMETHOD(X3)() = 0;
    STDMETHOD(X4)() = 0;
    STDMETHOD(X5)() = 0;
    STDMETHOD(X6)() = 0;
    STDMETHOD(X7)() = 0;
    STDMETHOD(X8)() = 0;
    STDMETHOD(X9)() = 0;
    STDMETHOD(X10)() = 0;
    STDMETHOD(X11)() = 0;
    STDMETHOD(X12)() = 0;
    STDMETHOD(X13)() = 0;
    STDMETHOD(X14)() = 0;
    STDMETHOD(X15)() = 0;
    STDMETHOD(X16)() = 0;
    STDMETHOD(X17)() = 0;
    STDMETHOD(X18)() = 0;
    STDMETHOD(GetTabletFromTabletContextId)(TabletId tcid, QWindowsInkTabletInterface **tablet) = 0;
    STDMETHOD(X20)() = 0;
    STDMETHOD(X21)() = 0;
    STDMETHOD(X22)() = 0;
    STDMETHOD(setDesiredPacketDescription)(ULONG, const GUID *) = 0;
    STDMETHOD(getDesiredPacketDescription)(ULONG *, const GUID **) = 0;
    STDMETHOD(getPacketDescriptionData)(TabletId tcid, float *scaleX, float *scaleY, ULONG *cProperties, PacketProperty **ppProperties) = 0;
};

struct QWindowsRealTimeStylusInterface3 : public IUnknown
{
	STDMETHOD(get_MultiTouchEnabled)(BOOL *) = 0;
	STDMETHOD(put_MultiTouchEnabled)(BOOL) = 0;
};

struct QWindowsStylusEventPluginInterface : public IUnknown
{
    STDMETHOD(realTimeStylusEnabled)(QWindowsRealTimeStylusInterface *piRtsSrc, ULONG cTcidCount, const TabletId *pTcids) = 0;
    STDMETHOD(realTimeStylusDisabled)(QWindowsRealTimeStylusInterface *piRtsSrc, ULONG cTcidCount, const TabletId *pTcids) = 0;
    STDMETHOD(stylusInRange)(QWindowsRealTimeStylusInterface *piRtsSrc, TabletId tcid, CursorId sid) = 0;
    STDMETHOD(stylusOutOfRange)(QWindowsRealTimeStylusInterface *piRtsSrc, TabletId tcid, CursorId sid) = 0;
    STDMETHOD(stylusDown)(QWindowsRealTimeStylusInterface *piRtsSrc, const StylusInfo *pStylusInfo, ULONG cPropCountPerPkt, LONG *pPacket, LONG **ppInOutPkt) = 0;
    STDMETHOD(stylusUp)(QWindowsRealTimeStylusInterface *piRtsSrc, const StylusInfo *pStylusInfo, ULONG cPropCountPerPkt, LONG *pPacket, LONG **ppInOutPkt) = 0;
    STDMETHOD(stylusButtonDown)(QWindowsRealTimeStylusInterface *piRtsSrc, CursorId sid, const GUID *pGuidStylusButton, POINT *pStylusPos) = 0;
    STDMETHOD(stylusButtonUp)(QWindowsRealTimeStylusInterface *piRtsSrc, CursorId sid, const GUID *pGuidStylusButton, POINT *pStylusPos) = 0;
    STDMETHOD(inAirPackets)(QWindowsRealTimeStylusInterface *piRtsSrc, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, ULONG *pcInOutPkts, LONG **ppInOutPkts) = 0;
    STDMETHOD(packets)(QWindowsRealTimeStylusInterface *piRtsSrc, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, ULONG *pcInOutPkts, LONG **ppInOutPkts) = 0;
    STDMETHOD(customStylusDataAdded)(QWindowsRealTimeStylusInterface *piRtsSrc, const GUID *pGuidId, ULONG cbData, const unsigned char *pbData) = 0;
    STDMETHOD(systemEvent)(QWindowsRealTimeStylusInterface *piRtsSrc, TabletId tcid, CursorId sid, SystemEvent event, SYSTEM_EVENT_DATA eventData) = 0;
    STDMETHOD(tabletAdded)(QWindowsRealTimeStylusInterface *piRtsSrc, IInkTablet *piTablet) = 0;
    STDMETHOD(tabletRemoved)(QWindowsRealTimeStylusInterface *piRtsSrc, LONG iTabletIndex) = 0;
    STDMETHOD(error)(QWindowsRealTimeStylusInterface *piRtsSrc, QWindowsStylusEventPluginInterface *piPlugin, QWindowsRealTimeStylusDataInterest dataInterest, HRESULT hrErrorCode, LONG_PTR *lptrKey) = 0;
    STDMETHOD(updateMapping)(QWindowsRealTimeStylusInterface *piRtsSrc) = 0;
    STDMETHOD(dataInterest)(QWindowsRealTimeStylusDataInterest *pDataInterest) = 0;
};

class QWindowsStylusEventPlugin : public QWindowsStylusEventPluginInterface
{
public:
    QWindowsStylusEventPlugin(HWND window);
    virtual ~QWindowsStylusEventPlugin();

    // IStylusAsyncPlugin methods

    // IStylusPlugin methods
    STDMETHOD(stylusDown)(QWindowsRealTimeStylusInterface* piSrcRtp, const StylusInfo* pStylusInfo, ULONG cPropCountPerPkt, LONG* pPacket, LONG**);
    STDMETHOD(stylusUp)(QWindowsRealTimeStylusInterface* piSrcRtp, const StylusInfo* pStylusInfo, ULONG cPropCountPerPkt, LONG* pPacket, LONG**);
    STDMETHOD(stylusInRange)(QWindowsRealTimeStylusInterface* piSrcRtp, TabletId tcid, CursorId cid);
    STDMETHOD(stylusOutOfRange)(QWindowsRealTimeStylusInterface* piSrcRtp, TabletId tcid, CursorId cid);
    STDMETHOD(dataInterest)(QWindowsRealTimeStylusDataInterest* pEventInterest);
    STDMETHOD(packets)(QWindowsRealTimeStylusInterface* piSrcRtp, const StylusInfo* pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG* pPackets, ULONG*, LONG**);
    STDMETHOD(inAirPackets)(QWindowsRealTimeStylusInterface*, const StylusInfo*, ULONG, ULONG, LONG*, ULONG*, LONG**);

    // Default imp
    STDMETHOD(realTimeStylusEnabled)(QWindowsRealTimeStylusInterface */*piRtsSrc*/, ULONG /*cTcidCount*/, const TabletId */*pTcids*/){ return S_OK; }
    STDMETHOD(realTimeStylusDisabled)(QWindowsRealTimeStylusInterface */*piRtsSrc*/, ULONG /*cTcidCount*/, const TabletId */*pTcids*/){ return S_OK; }
    STDMETHOD(stylusButtonUp)(QWindowsRealTimeStylusInterface*, CursorId, const GUID*, POINT*) { return S_OK; }
    STDMETHOD(stylusButtonDown)(QWindowsRealTimeStylusInterface*, CursorId, const GUID*, POINT*) { return S_OK; }
    STDMETHOD(systemEvent)(QWindowsRealTimeStylusInterface*, TabletId, CursorId, SystemEvent, SYSTEM_EVENT_DATA) { return S_OK; }
    STDMETHOD(tabletAdded)(QWindowsRealTimeStylusInterface*, IInkTablet*) { return S_OK; }
    STDMETHOD(tabletRemoved)(QWindowsRealTimeStylusInterface*, LONG) { return S_OK; }
    STDMETHOD(customStylusDataAdded)(QWindowsRealTimeStylusInterface*, const GUID*, ULONG, const BYTE*) { return S_OK; }
    STDMETHOD(error)(QWindowsRealTimeStylusInterface*, QWindowsStylusEventPluginInterface*, QWindowsRealTimeStylusDataInterest, HRESULT, LONG_PTR*) { return S_OK; }
    STDMETHOD(updateMapping)(QWindowsRealTimeStylusInterface*) { return S_OK; }

    // IUnknown methods
    STDMETHOD_(ULONG,AddRef)();
    STDMETHOD_(ULONG,Release)();
    STDMETHOD(QueryInterface)(REFIID riid, LPVOID *ppvObj);

private:
    QPointF scaleCoordinates(QWindowsRealTimeStylusInterface *realTimeStylus, int coordX, int coordY, TabletId tcid, const RECT &windowArea);
    QWindowsTabletInfo& queryTablet(QWindowsRealTimeStylusInterface *realTimeStylus, TabletId tcid);
    Qt::MouseButtons translatePacketStatus(long status) const;
    void processPackets(QWindowsRealTimeStylusInterface *realTimeStylus, const StylusInfo *pStylusInfo, ULONG cPktCount, ULONG cPktBuffLength, LONG *pPackets, bool isInAir, DWORD dwFlags);

    LONG m_refCount;
    IUnknown *m_marshaler;
    HWND m_window;
    QHash<TabletId, QWindowsTabletInfo> m_tabletInfos;
    QWindowsStylusPacketFormat m_packetFormat;
	QList<QWindowSystemInterface::TouchPoint> m_touchPoints;
};

class QWindowsWindowStylusHandler
{
public:
    QWindowsWindowStylusHandler(HWND window);
    ~QWindowsWindowStylusHandler();

    void setEnabled(bool enabled);
    bool enabled();
private:
    HWND m_window;
    QWindowsRealTimeStylusInterface *m_realTimeStylus;
    QWindowsStylusEventPlugin *m_stylusPlugin;
};

class QWindowsStylusHandler
{
public:
    explicit QWindowsStylusHandler();
    ~QWindowsStylusHandler();

    static QWindowsStylusHandler* create();

    QWindowsWindowStylusHandler* addWindow(HWND window);
    void removeWindow(HWND window);

    QString description();

private:
    QHash<HWND,QWindowsWindowStylusHandler*> m_windowHandlers;
};

QT_END_NAMESPACE

#endif // !QT_NO_TABLETEVENT && !Q_OS_WINCE
#endif // QWINDOWSSTYLUSHANDLER_H
